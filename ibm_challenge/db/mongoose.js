var mongoose = require("mongoose");
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/ibm_challenge');
mongoose.connection.on("error", (err) => {
    return console.log("Failed to connect with DB" + err);
});
module.exports = { mongoose }