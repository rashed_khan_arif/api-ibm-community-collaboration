const router = require('express').Router();
const User = require("../data/model/UserModel");
const bcrypt = require('bcrypt');

router.post('/login', function (req, res, next) {
    //bcrypt.hash(req.body.password, 10, function (err, hash) {
    User.find({email: req.body.email, password: req.body.password}, (err, doc) => {
        if (err) {
            res.status(400).send(err);
        } else {
            doc.removeProperty("password");
            res.send(doc);
        }
    });
    // });
});


router.post('/registration', function (req, res, next) {

    let user = new User();
    user.fullName = req.body.fullName;
    user.email = req.body.email;
    user.contactNo = req.body.contactNo;

    user.password = req.body.password;
    user.save().then((doc) => {
        console.log("Success");
        res.send(user);
    }).catch((er) => {
        console.log("Failed");
        res.send({msg: "Error on Registration !" + er})
    })

});

module.exports = router;