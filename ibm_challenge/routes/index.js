const router = require('express').Router();
router.get('/ping', function (req, res, next) {
    return res.json({
        data: "test"
    });
});

module.exports = router;
